# Storage Solutions

An inventory extension addon for [Deathstrider](https://gitlab.com/accensi/deathstrider).

---

This addon gives you some added ways of carrying a *practically infinite* amount of items with you, provided you have the mana/battery and time to spare in managing them.

## Deep Storage Device

For Flesh & Steel playthroughs, you can purchase the deep storage device at the shop. Inserting and removing items uses battery power. Choose what you withdraw carefully as time rifts may be opened by disrupting spacetime too much.

Energy consumption and rift agitation may not be balanced, but neither is being able to carry so much either.

## Astral Capsule

For mixed playthroughs, you can cast the spell and open a small gate. Items dropped on the ground within the gate's radius will be stored in a personal pocket realm until you cast the spell again. The gate can be manually closed by punching it, casting the spell again.

---

## Things to note

Leaving the level will attempt to close the Astral Capsule, but due to the order of things getting cleaned up, it's not guaranteed that it will get every item.

---

See [credits.txt](./credits.txt) for attributions.

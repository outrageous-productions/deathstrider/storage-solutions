AddOptionMenu DeathstriderMenuAddons
{
	SubMenu "OP Storage Solutions Options", "op_storagesolutions_options"
}

OptionValue "DSDPowerSupply"
{
	0, "Voidstones (appropriate)"
	1, "Any Batteries (classic)"
	2, "Flux Batteries only (alternative)"
}

OptionMenu "op_storagesolutions_options"
{
	Title "Storage Solutions Options"
	Option "Deep Storage Device is powered by", "OPDeepStorageDevicePowerSupply", "DSDPowerSupply"
	StaticText "Voidstone-powered DSDs are purposefully more powerful than the alternatives."
	StaticText "Takes effect for newly spawned DSDs."
}